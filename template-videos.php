<?php

/**
 * Template Name: Video Template
 */
?>
<?php while (have_posts()) : the_post();
$viewbook_page_type = get_post_meta($post->ID, '_viewbook_page_type', true);
$viewbook_page_position = get_post_meta($post->ID, '_viewbook_page_position', true);

if ($viewbook_page_type == "template-content") {
    get_template_part('templates/viewbook-page');
}
else {
    if ($viewbook_page_position == "fullscreen") {
        get_template_part('templates/viewbook-fullscreen');
    } 
    else {
        get_template_part('templates/viewbook-split');
    }
}
endwhile;

query_posts(array(
	'showposts' => 20,
	'post_type' => 'page',
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'meta_value' => 'template-video'
));
 while (have_posts()) {
	  	the_post();
	  	
	  	$viewbook_page_type        = get_post_meta( $post->ID, '_viewbook_page_type', true );
		$viewbook_page_position    = get_post_meta( $post->ID, '_viewbook_page_position', true );
		
	  	if($viewbook_page_type == "template-content"){
	  		get_template_part('templates/viewbook-page'); 
	  	}else{
	  		if($viewbook_page_position == "fullscreen"){
	  			get_template_part('templates/viewbook-fullscreen');
	  		}else{
	  			get_template_part('templates/viewbook-split'); 
	  		}
	  	}
	}
?>