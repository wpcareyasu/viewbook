<?php
	$viewbook_page_slug = $post->post_name;
	if (has_post_thumbnail( $post->ID ) ):
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
  		$image_mobile = get_post_meta( $post->ID, '_viewbook_page_fullscreen_mobile_crop', true );
?>
	<style>
	.pane-wrapper.<?php echo $viewbook_page_slug ?>{
		background-image: url('<?php echo $image_mobile; ?>');
	}
	@media (min-width: 768px){
		.pane-wrapper.<?php echo $viewbook_page_slug ?>{
	 		background-image: url('<?php echo $image[0]; ?>');
	  	}
	}
	</style>
<?php
	endif;
?>
<div class="pane-wrapper section-hero <?php echo $viewbook_page_slug ?>">
  		<div class="title-wrapper">
  		<hr>
  		  <?php get_template_part('templates/page', 'header'); ?>
  		  <?php get_template_part('templates/content', 'page'); ?>
  		</div>
</div>
