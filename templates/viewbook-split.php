<?php
	$viewbook_page_color                  = get_post_meta( $post->ID, '_viewbook_page_color', true );                                                                    
	$viewbook_page_video_url              = get_post_meta( $post->ID, '_viewbook_page_video_url', true );                                                    
	$viewbook_page_video_thumb            = get_post_meta( $post->ID, '_viewbook_page_video_thumb', true );                                            
	$viewbook_page_fullscreen_mobile_crop = get_post_meta( $post->ID, '_viewbook_page_fullscreen_mobile_crop', true );
	$viewbook_page_position               = get_post_meta( $post->ID, '_viewbook_page_position', true );                                                        
	$viewbook_page_type                   = get_post_meta( $post->ID, '_viewbook_page_type', true );                                                                        
	$viewbook_page_text 				  = get_post_meta( $post->ID, '_viewbook_page_text', true );                                                                                                                                                
	$viewbook_page_slug                   = $post->post_name;
	$viewbook_page_classes 				  = $viewbook_page_type . ' ' . $viewbook_page_text . ' ' . $viewbook_page_position;

	if (has_post_thumbnail( $post->ID )) :
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$page_image = $image[0];
	elseif($viewbook_page_video_thumb == '') :
		$page_image = $viewbook_page_video_thumb;
	endif;
	
?>

<div data-anchor="<?php echo $viewbook_page_slug; ?>" class="pane-wrapper <?php echo $viewbook_page_classes ?>" style="background-color: <?php echo $viewbook_page_color ?>;">
	<?php echo edit_post_link('Edit Page', '<div class="admin-addon">', '</div>'); ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 wrapper col-sm-6 <?php echo $viewbook_page_position === 'right' ? 'col-lg-push-6' : '' ?>">
				<div class="media-wrapper">
					<?php if($viewbook_page_video_url !== '') echo '<div class="fullscreen-wrapper"><div class="icon-wrapper"><a href="' . $viewbook_page_video_url . '" class="iframe-popup"><i class="fa fa-youtube-play"></i></a></div></div>' ?>
					<img src="<?php echo $page_image;?>" >
				</div>
			</div>
			<div class="col-lg-6 col-sm-6 wrapper <?php echo ($viewbook_page_position === 'right') ? 'col-lg-pull-6' : '' ?>">
				<div class="content-wrapper">
				<hr>
					<h2><?php echo $post->post_title; ?></h2>
					<div class="entry"><?php the_content(); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>