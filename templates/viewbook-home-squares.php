<?php
	$viewbook_page_slug = $post->post_name;
	$image2 = has_post_thumbnail($post->ID) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'homepage-squares' ) : '';
?>
<div data-anchor="<?php echo $viewbook_page_slug ?>" class="section <?php echo $viewbook_page_slug ?> col-lg-4 col-sm-6 col-xs-12">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="pane-wrapper square-link first-slide loading">
	  <div class="title-wrapper">
	  	<hr>
	    <div class="page-header">
  			<h2><?php echo the_title(); ?></h2>
		</div>
	    <?php get_template_part('templates/content', 'page'); ?>
	    <div class="media-wrapper">
			<img width="360" height ="195"src="<?php echo $image2[0]; ?>" style="width: 100%; height: auto;">
		</div>
	  </div>
	</a>
<?php

	$parent_id =$post->ID;

	$child_args = array(
		'showposts' => 20,
		'post_parent' => $parent_id,
		'post_type' => 'page',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$child_query = new WP_Query( $child_args );
	$vb_page_slides_images;

	if ( $child_query->have_posts() ) {
		while ( $child_query->have_posts() ) {
			$child_query->the_post();

	 		$viewbook_page_type = get_post_meta( $post->ID, '_viewbook_page_type', true );
			$viewbook_page_position    = get_post_meta( $post->ID, '_viewbook_page_position', true );
			$viewbook_page_color = get_post_meta( $post->ID, '_viewbook_page_color', true );                                
			$viewbook_page_text  = get_post_meta( $post->ID, '_viewbook_page_text', true );                                    
			$viewbook_page_slug = $post->post_name;

			$viewbook_page_video_url              = get_post_meta( $post->ID, '_viewbook_page_video_url', true );                                                    
			$viewbook_page_video_thumb            = get_post_meta( $post->ID, '_viewbook_page_video_thumb', true );                                            
			$viewbook_page_fullscreen_mobile_crop = get_post_meta( $post->ID, '_viewbook_page_fullscreen_mobile_crop', true );

	  		if ($viewbook_page_fullscreen_mobile_crop === "" && $viewbook_page_position === "fullscreen") {
	  			$page_slide_image = $viewbook_page_fullscreen_mobile_crop;
	  		} elseif (has_post_thumbnail($post->ID) && $viewbook_page_position === "fullscreen" ) {
	  			$page_slide_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail')[0];
	  		} else {
	  			$page_slide_image = "";
	  		}
			$viewbook_page_classes = 'pane-wrapper' . ' ' . $viewbook_page_type . ' ' . $viewbook_page_text . ' ' . $viewbook_page_position . ' loading';
?>
		<div data-anchor="<?php echo $viewbook_page_slug; ?>" class="<?php echo $viewbook_page_classes?>" style="background-color: <?php echo $viewbook_page_color ?>; <?php echo ($page_slide_image ? 'background-image: url(' . $page_slide_image  . ');' : '') ?>)"><div class="loading-wrapper"><i class="fa fa-refresh fa-spin"></i></div></div>
<?php
		}
	} else {
			// no posts found
	}
	if($parent_id !== 141 && $parent_id !== 38){
		$the_query2 = new WP_Query( 'pagename=footer' );
		// The Loop
		if ( $the_query2->have_posts() ) {
			while ( $the_query2->have_posts() ) {
				$the_query2->the_post();
				$viewbook_page_type = get_post_meta( $post->ID, '_viewbook_page_type', true );
				$viewbook_page_position = get_post_meta( $post->ID, '_viewbook_page_position', true );
				$viewbook_page_color = get_post_meta( $post->ID, '_viewbook_page_color', true );                                
				$viewbook_page_text  = get_post_meta( $post->ID, '_viewbook_page_text', true );                                    
				$viewbook_page_slug = $post->post_name;
				$viewbook_page_classes = 'pane-wrapper' . ' ' . $viewbook_page_type . ' ' . $viewbook_page_text . ' ' . $viewbook_page_position . ' loading';
?>
				<div data-anchor="<?php echo $viewbook_page_slug; ?>" class="<?php echo $viewbook_page_classes?>" style="background-color: <?php echo $viewbook_page_color ?>"><div class="loading-wrapper"><i class="fa fa-refresh fa-spin"></i></div></div>
<?php
			}
		} else {
			// no posts found
		}
		// Restore original Post Data
		wp_reset_postdata();
	}
?>
</div>