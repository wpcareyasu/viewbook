<footer class="content-info" role="contentinfo">
    <div class="container">
        <section class="widget widget_text">
          <div class="textwidget">
              <p><a href="https://wpcarey.asu.edu" target="_blank"><img width="370" height="99" src="<?php echo get_template_directory_uri(); ?>/dist/images/asu-wpcarey-logo-lg.png" alt="W. P. Carey School of Business | Arizona State University" scale="0"></a></p>
              <h5 class="screen-reader-text">W. P. Carey School of Business</h5>
              <p>&nbsp;</p>
              <div class="social-media">
                  <a href="http://facebook.com/wpcareyschool" target="_blank">
                      <i class="fa fa-2x fa-facebook"></i>
                  </a>
                  <a href="http://twitter.com/wpcareyschool" target="_blank">
                      <i class="fa fa-2x fa-twitter"></i>
                  </a>
                  <a href="http://www.linkedin.com/groups?gid=43848" target="_blank">
                      <i class="fa fa-2x  fa-linkedin"></i>
                  </a>
                  <a href="https://youtube.com/wpcareyschool" target="_blank">
                      <i class="fa fa-2x fa-youtube"></i>
                  </a>
                  <a href="https://instagram.com/wpcareyschool/" target="_blank">
                      <i class="fa fa-2x fa-instagram"></i>
                  </a>
              </div>
          </div>
        </section>
        <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
<div class="asu-footer">
    <div class="little-foot">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="little-foot-nav">
                        <li><a href="http://www.asu.edu/copyright/" target="_blank">Copyright &amp; Trademark</a></li>
                        <li><a href="http://www.asu.edu/accessibility/" target="_blank">Accessibility</a></li>
                        <li><a href="http://www.asu.edu/privacy/" target="_blank">Privacy</a></li>
                        <li><a href="https://cfo.asu.edu/hr-applicant" target="_blank">jobs@asu</a></li>
                        <li><a href="http://www.asu.edu/emergency/" target="_blank">Emergency</a></li>
                        <li class="no-border"><a href="http://www.asu.edu/contactasu/" target="_blank">Contact ASU</a></li>
                    </ul>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
</div>
