<?php
	$viewbook_page_color = get_post_meta( $post->ID, '_viewbook_page_color', true );                                
	$viewbook_page_text  = get_post_meta( $post->ID, '_viewbook_page_text', true );                                    
	$viewbook_page_type  = get_post_meta( $post->ID, '_viewbook_page_type', true );
	$viewbook_page_position = get_post_meta( $post->ID, '_viewbook_page_position', true );
	$viewbook_page_slug = $post->post_name;
	//$viewbook_page_classes = ($viewbook_page_slug !== 'take-your-next-step' ? 'pane-wrapper ': '') . ' ' . $viewbook_page_type . ' ' . $viewbook_page_text . ' ' . $viewbook_page_position;
	$viewbook_page_classes = 'pane-wrapper' . ' ' . $viewbook_page_type . ' ' . $viewbook_page_text . ' ' . $viewbook_page_position;
?>
<div data-anchor="<?php echo $viewbook_page_slug; ?>" class="<?php echo $viewbook_page_classes?>" style="background-color: <?php echo $viewbook_page_color ?>">
	<?php echo edit_post_link('Edit Page', '<div class="admin-addon">', '</div>'); ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 wrapper">
				<div class="content-wrapper">
					<hr>
					<h2><?php the_title(); ?></h2>
					<div class="entry"><?php the_content(); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>