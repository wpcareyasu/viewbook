<?php
	$viewbook_page_type        = get_post_meta( $post->ID, '_viewbook_page_type', true );                                
	$viewbook_page_position    = get_post_meta( $post->ID, '_viewbook_page_position', true );                        
	$viewbook_page_text 	   = get_post_meta( $post->ID, '_viewbook_page_text', true );                                              
	$viewbook_page_video_url   = get_post_meta( $post->ID, '_viewbook_page_video_url', true );
	$viewbook_page_slug = $post->post_name;
	$viewbook_pane_classes = $viewbook_page_type . ' ' . $viewbook_page_text . ' ' . $viewbook_page_position;

	if (has_post_thumbnail( $post->ID ) ):
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	endif;
?>
<div data-anchor="<?php echo $viewbook_page_slug; ?>" class="pane-wrapper <?php echo $viewbook_pane_classes ?>" <?php echo ($image ? 'style="background-image: url('. $image[0]. ');"' : ''); ?> >

<?php echo edit_post_link('Edit Page', '<div class="admin-addon">', '</div>'); ?>
<?php if($viewbook_page_video_url !== '') echo '<div class="fullscreen-wrapper"><div class="icon-wrapper"><a class="iframe-popup"  href="' . $viewbook_page_video_url . '"><i class="fa fa-youtube-play"></i></a></div></div>' ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 wrapper">
				<div class="content-wrapper">
				<hr>
					<h2><?php echo $post->post_title; ?></h2>
					<div class="entry"><?php the_content(); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>