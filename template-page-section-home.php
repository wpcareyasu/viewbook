<?php
/**
 * Template Name: Viewbook Section Home
 */
?>
<?php
	while (have_posts()) : the_post();
		get_template_part('templates/viewbook-hero');
  	endwhile;
?>
<div class="container-fluid">
<div class="section">
<?php
	$parent_id = $post->ID;

	$child_args = array(
		'showposts' => 20,
		'post_parent' => $parent_id,
		'post_type' => 'page',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$child_query = new WP_Query( $child_args );

	if ( $child_query->have_posts() ) {
		while ( $child_query->have_posts() ) {
			$child_query->the_post();
	 
	 		$viewbook_page_type = get_post_meta( $post->ID, '_viewbook_page_type', true );
			$viewbook_page_position    = get_post_meta( $post->ID, '_viewbook_page_position', true );
			
	  		if($viewbook_page_type == "template-content"){
	  			get_template_part('templates/viewbook-page'); 
	  		}else{
	  			if($viewbook_page_position == "fullscreen"){
	  				get_template_part('templates/viewbook-fullscreen');
	  			}else{
	  				get_template_part('templates/viewbook-split'); 
	  			}
	  		}
	  	}
	}

	if($parent_id !== 141){
		$the_query2 = new WP_Query( 'pagename=footer' );
		// The Loop
		if ( $the_query2->have_posts() ) {
			while ( $the_query2->have_posts() ) {
				$the_query2->the_post();
				get_template_part('templates/viewbook-split'); 
			}
		} else {
			// no posts found
		}

		//take your next steps popup
		$the_query2 = new WP_Query( 'page_id=147' );
		// The Loop
		if ( $the_query2->have_posts() ) {
			while ( $the_query2->have_posts() ) {
				$the_query2->the_post(); ?>
				<div id="next-steps" class="mfp-hide white-popup-block">
					<?php echo edit_post_link('Edit Page', '<div class="admin-addon">', '</div>'); ?>
					<div class="container">
						<div class="row">
							<div class="col-lg-12 wrapper">
								<div class="content-wrapper">
									<hr>
									<h2><?php the_title(); ?></h2>
									<div class="entry"><?php the_content(); ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }
		} else {
			// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
	}
?>
</div>
</div>
<div class="social-sharing">
<button><i class="fa fa-share-alt"></i></button>
<?php
	if ( function_exists( 'sharing_display' ) ) {
		sharing_display( '', true );
	}

	if ( class_exists( 'Jetpack_Likes' ) ) {
		$custom_likes = new Jetpack_Likes;
		echo $custom_likes->post_likes( '' );
	}
?>
</div>