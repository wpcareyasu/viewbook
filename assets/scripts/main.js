/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        var nextPage = $('.menu-item.active').next().find('a').attr('href');
        $('.btn.explore').attr('href', nextPage);
        $('.ajax-popup-link').magnificPopup({
          type: 'ajax',
          disableOn: function() {
            if ($(window).width() < 600) {
              return false;
            }
            return true;
          }
        });

        $(document).on('click', '.popup-modal-dismiss', function(event) {
          event.preventDefault();
          $.magnificPopup.close();
        });

        $(document).on('click', '.popup-modal', function(event) {
          event.preventDefault();
          var modalURL = $(event.target).attr('href');
          $.magnificPopup.open({
            items: {
              src: modalURL,
            },
            type: 'inline',
            preloader: true,
            closeBtnInside: true,
            removalDelay: 300,
            mainClass: 'mfp-fade',
          });
        });

        var is_mobile = false;

        if ($('footer').css('display') === 'none') {
          is_mobile = true;
        }

        var fetch_anchors = [];
        $('.menu-item').each(function() {
          var lol = $(this)
            .find('a')
            .attr('href')
            .replace(/^(?:\/\/|[^\/]+)*\//, '')
            .replace('index.php', '')
            .replace(/\//g, '');
          fetch_anchors.push(lol);
        });

        var firstImageSize, $headerNavBar, newSize, scrollFlag, socialSticky;

        function sectionCheck(url) {
          //console.log('fetch-anchors: ' + url);
          for (var i = 0; i < fetch_anchors.length; i++) {
            if (fetch_anchors[i] === url) {
              return true;
            }
          }
          return false;
        }

        function redirector(device, initial) {
          //console.log('redirector: ' + device);
          //example http://viewbook.dev/where-your-future-starts/second-page
          //example http://viewbook.dev/#where-your-future-starts/second-page
          var path = window.location.pathname;
          var splitPath = path.split('/');
          var section = splitPath[1];
          var hash = window.location.hash;
          var is_root = location.pathname === "/";

          if (is_root && hash) {
            //console.log('special');
            section = hash.slice(1).split('/')[0];
          }

          var sectionPass = sectionCheck(section);
          var mobilePass = device === 'mobile' && sectionPass && !hash;
          var desktopPass = device === 'desktop' && sectionPass && hash && is_root;

          //console.log('section: ' + section);
          //console.log('device: ' + device);
          //console.log('is_root: ' + is_root);
          //console.log('hash: ' + hash);
          //console.log('sectionCheck: ' + sectionPass);

          //is device mobile & sectionPass & has no hash
          if (mobilePass) {
            //console.log('redirector2: mobile');
            newURL = window.location.origin + '#';
            $.each(splitPath, function(index, value) {
              if (index > 1) {
                newURL += '/';
              }
              newURL += value;
            });
            //console.log('mobile: ' + newURL);
            window.location = newURL;
          } else if (desktopPass) {
            //console.log('redirector2: desktop');
            var hashToPage = hash.slice(1);
            //console.log('redirector2: hashToPage: ' + hashToPage);
            var something = hashToPage.split('/');
            //console.log('redirector2: something: ' + something);
            newURL = window.location.origin + '/' + something[0];
            //console.log('desktop: ' + newURL);
            window.location = newURL;
          } else if (!hash && is_root && initial) {
            location.reload();
          }
        }

        function scrollChange() {
          if ($(document).scrollTop() > firstImageSize && scrollFlag) {
            scrollFlag = false;
            socialSticky.addClass('sticky').css('top', $headerNavBar.outerHeight() + 20);
            if ($headerNavBar.data('size') === 'big') {
              $headerNavBar.data('size', 'small')
                .addClass('shrink')
                .addClass('animate');
            }
          } else if ($(document).scrollTop() < firstImageSize && !scrollFlag) {
            scrollFlag = true;
            socialSticky.removeClass('sticky').css('top', newSize);
            if ($headerNavBar.data('size') === 'small') {
              $headerNavBar.data('size', 'big')
                .removeClass('shrink')
                .addClass('animate');
            }
          }
        }

        var deviceFlag = is_mobile ? 'mobile' : 'desktop';

        if (is_mobile) {
          redirector('mobile', false);
          //if size of windows is bigger than mobile reload site
        } else {
          redirector('desktop', false);

          $('.social-sharing').hover(
            function() {
              $(this).children('.sharedaddy').toggleClass('toggle');
            },
            function() {
              $(this).children('.sharedaddy').toggleClass('toggle');
            }
          );

          $('header.navbar').data('size', 'big');
          firstImageSize = $('.section-hero').outerHeight();
          $headerNavBar = $('header.navbar');

          newSize = $headerNavBar.outerHeight() + firstImageSize + 20;
          $('.social-sharing').css('top', newSize);

          scrollFlag = true;
          socialSticky = $('.social-sharing');
          scrollChange();

          $(window).scroll(function() {
            scrollChange();
          });
          $('.iframe-popup').magnificPopup({
            type: 'iframe',
            iframe: {
              markup: '<div class="mfp-iframe-scaler">' +
                '<div class="mfp-close"></div>' +
                '<iframe width="1280" height="720" frameborder="0" allowfullscreen ebkitallowfullscreen mozallowfullscreen class="mfp-iframe"></iframe>' +
                '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
              patterns: {
                youtube: {
                  index: 'youtube.com/',
                  id: 'v=',
                  src: 'https://www.youtube.com/embed/%id%?autoplay=1&amp;rel=0&amp;VQ=HD720'
                },
                vimeo: {
                  index: 'vimeo.com/',
                  id: '/',
                  src: 'https://player.vimeo.com/video/%id%?autoplay=1&badge=0&byline=0'
                }
              }
            },
            disableOn: 600
          });
        }
        $(window).on('resize', function() {
          //if footer is visible && deviceFlag isn't desktop
          if ($('footer').css('display') !== 'none' && deviceFlag !== 'desktop') {
            //console.log('resize: Desktop');
            deviceFlag = 'desktop';
            $('body').html('');
            redirector('desktop', true);
          }
          //if footer is hidden && deviceFlag isn't mobile
          else if ($('footer').css('display') === 'none' && deviceFlag !== 'mobile') {
            //console.log('resize: Mobile');
            deviceFlag = 'mobile';
            $('body').html('');
            redirector('mobile', true);
          }
        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
        var fetch_anchors = [];
        var initNav = true;

        $('.section').each(function() {
          fetch_anchors.push($(this).attr('data-anchor'));
        });

        function updateNavs(sDirection) {
          //console.log(sDirection);
          var $currentSlide = $('.fp-section.active .fp-slide.active');
          var nextSlide;
          //console.log('updateNavs: ' + (sDirection ? sDirection : 'none'));
          if (sDirection) {
            if (sDirection === 'right') {
              nextSlide = $currentSlide.next();
            } else if (sDirection === 'left') {
              nextSlide = $currentSlide.prev();
            }
          } else {
            initNav = false;
            nextSlide = $currentSlide;
          }

          var lightConditions = ['fullscreen', 'light', 'footer'];
          var firstSlide = nextSlide.hasClass('first-slide');

          var light = false;
          for (var i = 0; i < lightConditions.length; i++) {
            if (nextSlide.hasClass(lightConditions[i])) {
              light = true;
              break;
            }
          }

          if (firstSlide) {
            $('.fp-slidesNav.top').hide().removeClass('animate;');
            $('#fp-nav').delay('500').fadeIn().addClass('animate');
            if (nextSlide.parents('.fp-section').is(':last-child')) {
              $('.fp-custom-controlArrow.fp-down').fadeOut('fast');
            } else {
              $('.fp-custom-controlArrow.fp-down').fadeIn();
            }
          } else {
            $('.fp-slidesNav.top').fadeIn().addClass('animate');
            $('#fp-nav').hide().removeClass('animate');
            $('.fp-custom-controlArrow.fp-down').fadeOut('fast');
          }

          if (light || firstSlide) {
            $('.fp-slidesNav').addClass('light');
          } else {
            $('.fp-slidesNav').removeClass('light');
          }
        }

        function fetchSlide(anchorLink, slide) {
          slide.removeClass('loading');
          var sectionLink = slide.siblings('a.first-slide').attr('href');
          var slideName = slide.attr('data-anchor');
          $.get(sectionLink, function(data) {
            var content = $(
              '.pane-wrapper[data-anchor="' + slideName + '"]', $(data)
            );
            var styles = content.attr('style');
            styles = styles ? styles.split(';') : '';
            //if (styles !== '') {
            //  $.each(styles, function(index, style) {
            //    if (style !== '') {
            //      style = style.split(':');
            //      slide.css(style[0], style[1]);
            //    }
            //  });
            //}
            slide.html(content.html());
            $.fn.fullpage.reBuild();
          });
        }

        function loadSlide(anchorLink) {
          var $currentSlide = $('.section.fp-section.active .fp-slide.active');
          var nextSlide = $currentSlide.next();
          if ($currentSlide.hasClass('loading') &&
            !$currentSlide.hasClass('first-slide')) {
            fetchSlide(anchorLink, $currentSlide);
          }
          if (nextSlide.hasClass('loading')) {
            fetchSlide(anchorLink, nextSlide);
          }
        }



        var fullPageOptions = {

          //Navigation
          anchors: fetch_anchors,
          navigation: true,
          navigationPosition: 'right',
          slidesNavigation: true,
          slidesNavPosition: 'top',

          //Scrolling
          //css3: true,
          //scrollingSpeed: 1000,
          autoScrolling: true,
          fitToSection: true,
          //scrollBar: true,
          //easing: 'easeInOutCubic',
          //easingcss3: 'ease',
          //loopBottom: false,
          //loopTop: false,
          loopHorizontal: false,
          //continuousVertical: false,
          normalScrollElements: '.col-lg-6.wrapper .content-wrapper, .mfp-wrap',
          scrollOverflow: true,
          touchSensitivity: 15,
          normalScrollElementTouchThreshold: 3,

          //Design
          controlArrows: true,
          verticalCentered: false,
          //resize : false,
          //paddingTop: '4.79em',
          //paddingBottom: '10px',
          fixedElements: 'header.navbar',
          animateAnchor: false,

          //Custom selectors
          //sectionSelector: '.section',
          slideSelector: '.pane-wrapper',

          //events
          onLeave: function(index, nextIndex, direction) {
            nextIndex--;

            $.fn.fullpage.silentMoveTo(index, 0);

            var downArrow = $('.fp-custom-controlArrow.fp-down');
            var rightArrow = $('.fp-section.active .fp-controlArrow.fp-next');
            var $currentSlide;

            if (downArrow.hasClass('flash')) {
              downArrow.removeClass('flash');
            }
            if (rightArrow.hasClass('flash')) {
              rightArrow.removeClass('flash');
            }

            $currentSlide = $('.fp-section:eq(' + nextIndex + ')').find('.fp-slide.active');
            $('#menu-main-menu .menu-item').removeClass('active');
            $('.menu-item a[href="' + $currentSlide.attr('href') + '"]').parent().addClass('active');

            if ($currentSlide.hasClass('first-slide') &&
              !$currentSlide.parents('.section').is(':last-child')) {
              downArrow.fadeIn('fast');
            } else {
              downArrow.fadeOut();
            }
          },
          afterLoad: function(anchorLink, index) {
            var rightArrow = $('.fp-section.active .fp-controlArrow.fp-next');
            if (!rightArrow.hasClass('flash')) {
              rightArrow.addClass('flash');
            }
          },
          afterRender: function() {
            var $currentSlide = $('.fp-section.active .fp-slide.active');
            $('#menu-main-menu .menu-item').removeClass('active');
            $('.menu-item a[href="' + $currentSlide.attr('href') + '"]').parent().addClass('active');

          },
          afterResize: function() {},
          afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
            var $currentSlide = $('.fp-section.active .fp-slide.active');
            var currentAnchor = $currentSlide.attr('data-anchor');
            loadSlide(currentAnchor);
            if (!initNav) {
              updateNavs();
            }
            var allowScroll = slideIndex > 0 ? true : false;
            if (allowScroll) {
              $.fn.fullpage.setAllowScrolling(false, 'down, up');
            } else {
              $.fn.fullpage.setAllowScrolling(true, 'down, up');
            }
          },
          onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {
            //console.log('Leave to: ' + nextSlideIndex);
            if (nextSlideIndex > 0) {
              updateNavs(direction);
            } else {
              updateNavs();
            }
          }
        };

        var is_mobile = false;
        if ($('footer').css('display') === 'none') {
          is_mobile = true;
        }

        if (is_mobile === true) {
          //create Take Your Next Steps in Mobile Menu
          var $menu = $('#menu-main-menu');
          var $menuItem = $menu.find('li:last-child').clone(false);
          $menuItem
            .find('a')
            .attr('href', '#where-your-future-starts/take-your-next-step')
            .html('Take Your Next Step');
          $menu.append($menuItem);

          $menu.find('a').click(function(event) {
            event.preventDefault();
            var menuItem = $(this).attr('href');
            if (menuItem.indexOf("#") > -1) {
              window.location.hash = menuItem;
            } else {
              var name = menuItem.match(/([^\/]*)\/*$/)[1];
              window.location.hash = name;
              //updateNavs();
            }
            $('nav').collapse('hide');
          });
          $(document).on('click', '.iframe-popup', function(event) {
            event.preventDefault();
            var videoURL = $(this).attr('href');
            var youtubeReg = /(watch\?v=)([a-zA-Z0-9\-_]+)/;
            var vimeoReg = /https:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
            var ytMatches = videoURL.match(youtubeReg);
            var viMatches = videoURL.match(vimeoReg);
            if (ytMatches) {
              window.location.href = 'https://www.youtube.com/embed/' +
                ytMatches[2] +
                '?autoplay=1&amp;rel=0&amp;VQ=HD720';
            } else if (viMatches) {
              window.location.href = '//player.vimeo.com/video/' +
                viMatches[2] +
                '?autoplay=1&badge=0&byline=0';
            }
          });
          $('main > .container').fullpage(fullPageOptions);
          $('.fp-custom-controlArrow.fp-down').click(function() {
            $(this).toggleClass('spin');
            $.fn.fullpage.moveSectionDown();
            $(this).removeClass('flash');
          });

          $('a.square-link').click(function(event) {
            event.preventDefault();
          });

          $(document).on('click', '.btn.explore', function() {
            $.fn.fullpage.moveSectionDown();
          });

          //Close the menu when clicked outside of area
          $(document).on('touchstart', function(e) {
            var container = $('#menu-main-menu');
            if (!container.is(e.target) && container.has(e.target).length === 0 && $('.wrap.shadow').length === 1) {
              $('nav').collapse('hide');
              e.preventDefault();
            }
          });

          //when nav is clicked cast a shadow over the page
          $('nav').on('show.bs.collapse', function() {
            $('.wrap').addClass('shadow');
            $('.navbar-toggle').toggleClass('collapsed');
          }).on('hide.bs.collapse', function(e) {
            $('.wrap').removeClass('shadow');
            $('.navbar-toggle').toggleClass('collapsed');
          });
        }
      },
      // About us page, note the change from about-us to about_us.
      'about_us': {
        init: function() {
          // JavaScript to be fired on the about us page
        }
      }
    }
  };
  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
