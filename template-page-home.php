<?php
/**
 * Template Name: Viewbook Home
 */
  while (have_posts()) : the_post();
    get_template_part('templates/viewbook-hero');
  endwhile;

?>

<div class="container squares">

<?php

  wp_reset_postdata();
  $descendants = get_pages(array());
  $incl = "";
  $exclude_ids = array(45);
  
  foreach ($descendants as $page) {
    if (($page->post_parent == $post->post_parent) || ($page->post_parent == $post->ID)){
     if(!in_array($page->ID, $exclude_ids)){
       $incl[] .= $page->ID;
     }
    }
  }

  $vb_top_pages = array('post_type' => 'page','post__in' => $incl,'orderby' => 'menu_order','showposts' => 7);
  $vb_top_pages_query = new WP_Query( $vb_top_pages );
  $vb_top_pages_images;

  // The Loop
  if ( $vb_top_pages_query->have_posts() ) {
    while ( $vb_top_pages_query->have_posts() ) {
      $vb_top_pages_query->the_post();
      $vb_page_slug = $post->post_name;
      if (has_post_thumbnail($post->ID)):
        $vb_top_pages_images['default'][$vb_page_slug] = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail')[0];
        $vb_top_pages_images['mobile'][$vb_page_slug] = get_post_meta($post->ID, '_viewbook_page_fullscreen_mobile_crop', true);
      endif;
      get_template_part('templates/viewbook-home-squares'); 
    }
  } else {
    // no posts found
  }
?>
</div>
<?php
  echo '<style>';
  foreach($vb_top_pages_images['mobile'] as $page => $image_url) {
    echo '.' . $page . ' .fp-slidesContainer > a {';
    echo 'background-image: url(' , $image_url !== '' ? $image_url : $vb_top_pages_images['default'][$page], ');';
    echo '}';
  }
  echo '</style>';
  wp_reset_postdata();

  //take your next steps popup
  $the_query2 = new WP_Query( 'page_id=147' );
  // The Loop
  if ( $the_query2->have_posts() ) {
    while ( $the_query2->have_posts() ) {
      $the_query2->the_post();
?>
      <div id="next-steps" class="mfp-hide white-popup-block">
        <?php echo edit_post_link('Edit Page', '<div class="admin-addon">', '</div>'); ?>
        <div class="container">
          <div class="row">
            <div class="col-lg-12 wrapper">
              <div class="content-wrapper">
                <hr>
                <h2><?php the_title(); ?></h2>
                <div class="entry"><?php the_content(); ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
    }
  }
?>
<div class="fp-custom-controlArrow fp-down flash"><span></span></div>